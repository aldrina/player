//global variables
var audioPlayer = document.getElementById('radio');// new Audio();
var songTitle = document.getElementById("song-title");
var fillBar = document.getElementById("seek-bar");
var volumeSlider = document.getElementById("volumeSlider");
//var canvas = document.getElementById("progress").getContext('2d');
var currentPlayingIndex = 0;


// init songs array
var songs = [
    "media/test.mp3",
    "media/ambiotic_snot.wav",
    "media/dipstick2.wav",
    "media/donut.wav",
    "media/earworm.wav",
    "media/guudshyte2.wav",
    "media/hairy_callihari.wav",
    "media/jolopy-crunch-Sequenced.wav",
    "media/nutso.wav",
    "media/lobrow.wav",
    "media/oddity.wav",
    "media/plucked.wav",
    "media/pardon.wav",
    "media/rubberbands.wav",
    "media/savior.wav",
    "media/trunkload.wav"
];

// play button
var isPlayerPlaying = false;
var isPaused = false;
var playBtn = document.getElementById('play');
playBtn.onclick = function () {
    if (isPlayerPlaying) {
        playBtn.src = "img/play.png";
        audioPlayer.pause();
        isPlayerPlaying = false;
        isPaused = true;
    } else {
        playBtn.src = "img/pause.png";
        playSong(currentPlayingIndex);
        isPaused = false;
    }
};

//prev button
var prevBtn = document.getElementById('previous');
prevBtn.onclick = function () {
    if (!isPlayerPlaying) {
        return;
    }
    currentPlayingIndex--;
    playSong(currentPlayingIndex);
}

//next button
var nextBtn = document.getElementById('forward');
nextBtn.onclick = function () {

    if (!isPlayerPlaying) {
        return;
    }
    currentPlayingIndex++;
    playSong(currentPlayingIndex);
}

function playSong(index) {
    var i = 0;
    if (index > 0 && index < songs.length) {
        i = index;
    } else if (index < 0) {
        i = songs.length - 1;
    } else {
        i = 0;
    }
    currentPlayingIndex = i;
    var song = songs[i];

    if (isPaused) {
        audioPlayer.play();
    } else {
        audioPlayer.src = song;
        audioPlayer.play();
    }
    isPlayerPlaying = true;
    var songNameWithExtension = audioPlayer.src.substring(audioPlayer.src.lastIndexOf('/') + 1); //return name.mp3
    var justName = songNameWithExtension.substring(0, songNameWithExtension.lastIndexOf('.'));
    songTitle.innerHTML = justName;
}

audioPlayer.addEventListener('timeupdate', onTimeUpdate);
function onTimeUpdate() {
    if(!audioPlayer.ended){
        var position = parseInt(audioPlayer.currentTime * (fillBar.width / audioPlayer.duration));
        fillBar.style.width = position * 100 + '%';
        var songCurrentTime = document.getElementById("song-current-time");
        songCurrentTime.innerHTML = getSongDuration(audioPlayer.currentTime);
        updateBar();
    }
}

var ff = document.getElementById('actualProgress');
function updateBar(){
    var a  = audioPlayer.currentTime *( 370 / audioPlayer.duration);
    ff.style.width = parseInt(a)+"px";
    ff.style.height = "10px";//parseInt(a)+"px";
}


//try to play next song once the current one finished
audioPlayer.addEventListener('ended', function () {
    console.log("ended");
    currentPlayingIndex++;
    playSong(currentPlayingIndex);
});


//on seek-bar click
var bar = document.getElementById('progress');

bar.addEventListener('click', clickedBar, false);

function clickedBar(e) {
    if(!audioPlayer.ended === false){
        var mouseX = e.pageX - bar.offsetLeft;
        var newTime = (mouseX*audioPlayer.duration)/fillBar;

        audioPlayer.currentTime = newTime;
        fillBar.style.width = mouseX + 'px';
    }
}

//returns song duration as a sting
function getSongDuration(currentSeconds) {
    var min = 0;
    var sec = 0;

    if ((currentSeconds / 60) > 0) {
        min = Math.floor(currentSeconds / 60);
    }
    if (currentSeconds > 60) {
        sec = Math.floor(currentSeconds % 60);
    } else {
        sec = Math.floor(currentSeconds);
    }

    //make min and sec two digits
    if (min <= 9) {
        min = "0" + min;
    }
    if (sec <= 9) {
        sec = "0" + sec;
    }
    return (min + ":" + sec);
}

//set volume
mutebtn.addEventListener("click",audmute,false);
//volumeslider.addEventListener("onChange",setvolume,false);

var lastVolumeValue = 0.5;
function audmute(){
    if(audioPlayer.muted){
        volumeSlider.value = lastVolumeValue;
        audioPlayer.muted = false;
        mutebtn.src = "img/volume-indicate.png";
    } else {
        lastVolumeValue = volumeSlider.value;
        volumeSlider.value = 0.0;

        audioPlayer.muted = true;
        mutebtn.src = "img/mute.png";
    }
}


function setvolume(){
    audioPlayer.muted = false;
    audioPlayer.volume = volumeSlider.value;

    if(volumeSlider.value == 0){
        mutebtn.src = "img/mute.png";
    }else{
        mutebtn.src = "img/volume-indicate.png";
    }
}